package za.co.sb.devserver.clients.feature;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * Holds the overall health and the details of the various checks
 * @author steven
 */
public class Health {
    
    @Schema(description = "The overall outcome of the health check. Should be UP or DOWN")
    String outcome;
    
    @Schema(description = "An array of each check done, with it's name, status (UP, DOWN) and optionally data elements")
    HealthCheck[] checks;

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public HealthCheck[] getChecks() {
        return checks;
    }

    public void setChecks(HealthCheck[] checks) {
        this.checks = checks;
    }
    
    
}
