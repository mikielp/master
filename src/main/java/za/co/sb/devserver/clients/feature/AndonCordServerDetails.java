package za.co.sb.devserver.clients.feature;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * 
 * @author steven
 */
public class AndonCordServerDetails {
    
    @Schema(description = "The name of the server.")
    private String serverName;
    
    @Schema(description = "The URL to use to notify for the Andon Cord on this server.")
    private String andonCordChangeURL;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getNotificationURL() {
        return andonCordChangeURL;
    }

    public void setNotificationURL(String notificationURL) {
        this.andonCordChangeURL = notificationURL;
    }
    
    
}
