/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import za.co.sb.devserver.rest.vo.HealthChecksVO;
import za.co.sb.devserver.services.HealthService;

/**
 *
 * @author steven
 */
@ApplicationScoped
@Path("health")
public class HealthResource {
    
    @Inject
    HealthService healthService;
    
    @GET
    @Operation(
            summary = "Call the health check against all registered servers",
            description = "Calls health check against all registered servers and returns the results for each one")
    @APIResponse(responseCode = "200",
            description = "Results of the health check request to all servers registered.",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            type = SchemaType.OBJECT,
                            implementation = HealthChecksVO.class)))
    public Response doHealthChecks() {
        healthService.doHealthChecks();
        return Response.ok().build();
    }
}
