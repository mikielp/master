/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.sb.devserver.pojo;

import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author steven
 */
public class PojoTest {
    
    public PojoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


  // The package to be tested
  private String packageName = "za.co.sb.devserver.rest.vo";

  @Test
  public void validateRESTVO() {
      Validator validator = ValidatorBuilder.create()
                            .with(new SetterMustExistRule(),
                                  new GetterMustExistRule())
                            .with(new SetterTester(),
                                  new GetterTester())
                            .build();
    validator.validate(packageName);
  }
  
  @Test
  public void validateEntity() {
      Validator validator = ValidatorBuilder.create()
                            .with(new SetterMustExistRule(),
                                  new GetterMustExistRule())
                            .with(new SetterTester(),
                                  new GetterTester())
                            .build();
    validator.validate("za.co.sb.devserver.entity");
  }
}
